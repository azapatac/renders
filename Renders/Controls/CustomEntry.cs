﻿namespace Renders.Controls
{
    using Xamarin.Forms;

    public class CustomEntry : Entry
    {
        public CustomEntry()
        {
            HeightRequest = 40;
        }
    }
}